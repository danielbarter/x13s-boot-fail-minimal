{
  inputs = {

    # for some reason, qemu isn't currently cached for nixos-unstable, so using a recent commit where I have it built
    nixpkgs.url = "github:NixOS/nixpkgs/b211b392b8486ee79df6cdfb1157ad2133427a2";
    nixos-x13s.url = "git+https://codeberg.org/adamcstephens/nixos-x13s";

  };

  outputs = {
      self, nixpkgs, nixos-x13s
  }: {
    nixosConfigurations = {
        fails-to-boot = nixpkgs.lib.nixosSystem {
          system = "aarch64-linux";
          modules = [

            nixos-x13s.nixosModules.default {
              nixos-x13s = {
                enable = true;
                kernel = "mainline";
              };

            }

            ({modulesPath,...}: {
              imports = [
                "${toString modulesPath}/installer/cd-dvd/iso-image.nix"
              ];
              nixpkgs.config.allowUnfree = true;
              isoImage = {
                makeEfiBootable = true;
                makeUsbBootable = true;
              };
            })

          ];
        };
    };

    packages."x86_64-linux" =
      let
        pkgs = import nixpkgs { system = "x86_64-linux"; };

        aarch64-vm = iso: let
          drive-flags = "format=raw,readonly=on";
          efi-flash = "${pkgs.pkgsCross.aarch64-multiplatform.OVMF.fd}/AAVMF/QEMU_EFI-pflash.raw";
        in pkgs.writeScriptBin "aarch64-run-nixos-vm" ''
             #!${pkgs.runtimeShell}
             ${pkgs.qemu_full}/bin/qemu-system-aarch64 \
             -machine virt \
             -cpu cortex-a57 \
             -m 2G \
             -smp 4 \
             -nographic \
             -drive if=pflash,file=${efi-flash},${drive-flags} \
             -drive file=${iso}/iso/nixos.iso,${drive-flags}
            '';

      in {

        fails-to-boot-iso = self.nixosConfigurations.fails-to-boot.config.system.build.isoImage;
        fails-to-boot-vm = aarch64-vm self.packages."x86_64-linux".fails-to-boot-iso;
      };
  };
}
